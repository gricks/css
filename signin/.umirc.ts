import { defineConfig } from '@umijs/max';
import proxy from './config/proxy';
import routes from './config/routes';

const { REACT_APP_ENV } = process.env;

export default defineConfig({
  antd: {},
  access: {},
  model: {},
  initialState: {},
  request: {},
  locale: {
    default: 'zh-CN',
    baseSeparator: '-',
  },
  proxy: proxy[REACT_APP_ENV || 'dev'],
  layout: {
    title: 'Identify',
  },
  routes: routes,
  npmClient: 'yarn',
});
