import React, { useEffect, useCallback } from 'react';
import { getUrlParams } from '@/utils/utils';

import styles from './index.less';

const DingtalkSignIn: React.FC = () => {
  const loadScript = (path: string) => {
    let scrs = document.getElementsByTagName('script');
    let last = scrs[scrs.length - 1];
    let scr = document.createElement('script');
    scr.innerHTML = `
		  !function (window, document) {
        function d(a) {
          var e, c = document.createElement("iframe"),
            d = "https://login.dingtalk.com/login/qrcode.htm?goto=" + a.goto ;
            d += a.style ? "&style=" + encodeURIComponent(a.style) : "",
            d += a.href ? "&href=" + a.href : "",
            c.src = d,
            c.frameBorder = "0",
            c.allowTransparency = "true",
            c.scrolling = "no",
            c.width =  a.width ? a.width + 'px' : "210px",
            c.height = a.height ? a.height + 'px' : "253px",
            e = document.getElementById(a.id),
            e.innerHTML = "",
            e.appendChild(c)
        }
        window.DDLogin = d
      }(window, document);

      var url = encodeURIComponent('https://paasv2.papegames.com/o/user-center/api/login/dingtalk/verify_code/?c_url=${path}');
      var qrcodeUrl = encodeURIComponent('https://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid=dingoacmbcqxsywqpazuec&response_type=code&scope=snsapi_login&state=STATE&redirect_uri='+url)
      var jump_type = "location"

      var obj = DDLogin({
        id: "qrcode",
        goto: qrcodeUrl,
        style: "border:none;background-color:#FFFFFF;",
        width : "210px",
        height: "253px"
      });

      var handleMessage = function (event) {
        var origin = event.origin;
        if(origin === "https://login.dingtalk.com") {
          var loginTmpCode = event.data;
          var gotoUrl = decodeURIComponent(qrcodeUrl) + "&loginTmpCode=" + loginTmpCode;
          console.log(gotoUrl, jump_type)

          if (jump_type == "iframe") {
            document.getElementById("qrcode").getElementsByTagName("iframe")[0].src = gotoUrl;
          } else {
            window.location.href = gotoUrl;
          }
        }
      };

      if (typeof window.addEventListener != 'undefined') {
        window.addEventListener('message', handleMessage, false);
      } else if (typeof window.attachEvent != 'undefined') {
        window.attachEvent('onmessage', handleMessage);
      }
    `;
    scr.async = true;
    last?.parentNode?.insertBefore(scr, last);
  };

  useEffect(() => {
    const urlParams = getUrlParams();
    loadScript(urlParams.redirect);
  }, []);

  const listenerResize = useCallback(() => {
    let n = document.documentElement.clientWidth;
    if (n <= 1280) {
      n = 1280;
    }
    setScale(n / 1920);
  }, []);

  useEffect(() => {
    listenerResize();
    window.addEventListener('resize', listenerResize);
  }, [listenerResize]);

  return (
    <div className={styles.dingtalkWrapper}>
      <div id="qrcode"></div>
    </div>
  );
};

export default DingtalkSignIn;
