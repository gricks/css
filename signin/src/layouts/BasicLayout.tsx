import {
  AppstoreOutlined,
  GithubFilled,
  InfoCircleFilled,
  PlusCircleFilled,
  QuestionCircleFilled,
  SearchOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons';
import ProCard from '@ant-design/pro-card';
import type { ProSettings } from '@ant-design/pro-layout';
import { PageContainer, ProLayout } from '@ant-design/pro-layout';
import { Outlet } from '@umijs/max';
import { Col, Divider, Input, Row } from 'antd';
import React, { useState } from 'react';

export default () => {
  const [settings] = useState<Partial<ProSettings> | undefined>({
    fixSiderbar: true,
    layout: 'top',
    splitMenus: true,
  });

  return (
    <div
      id="pro-layout"
      style={{
        height: '100vh',
      }}
    >
      <ProLayout
        title={false}
        pageTitleRender={false}
        logo={<div></div>}
        logoStyle={{ display: 'none' }}
        token={{
          header: {
            colorBgHeader: '#000',
            colorHeaderTitle: '#fff',
            colorTextMenu: '#dfdfdf',
            colorTextMenuSecondary: '#dfdfdf',
            colorTextMenuSelected: '#fff',
            colorBgMenuItemSelected: '#22272b',
            colorTextRightActionsItem: '#dfdfdf',
          },
        }}
        avatarProps={{
          src: 'https://gw.alipayobjects.com/zos/antfincdn/efFD%24IOql2/weixintupian_20170331104822.jpg',
          size: 'small',
          title: (
            <div
              style={{
                color: '#dfdfdf',
              }}
            >
              七妮妮
            </div>
          ),
        }}
        actionsRender={(props) => {
          return [
            props.layout !== 'side' && document.body.clientWidth > 900 ? (
              <div
                key="SearchOutlined"
                aria-hidden
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  marginInlineEnd: 24,
                }}
              >
                <Input
                  style={{
                    borderRadius: 4,
                    marginInlineEnd: 12,
                    backgroundColor: 'rgba(255,255,255,1)',
                    width: '210px',
                    color: '#dfdfdf',
                  }}
                  prefix={
                    <SearchOutlined
                      style={{
                        color: 'rgba(0,0,0,0.8)',
                      }}
                    />
                  }
                  bordered={false}
                />
                <PlusCircleFilled
                  style={{
                    color: 'rgba(255,255,255,1)',
                    fontSize: 24,
                  }}
                />
              </div>
            ) : undefined,
            <InfoCircleFilled key="InfoCircleFilled" />,
            <QuestionCircleFilled key="QuestionCircleFilled" />,
            <GithubFilled key="GithubFilled" />,
          ];
        }}
        headerContentRender={(_, defaultDom) => {
          return (
            <>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  marginInlineEnd: 52,
                }}
              >
                <Divider
                  style={{
                    height: '1.5em',
                    backgroundColor: '#dfdfdf',
                    marginRight: '24px',
                  }}
                  type="vertical"
                />
                <Row
                  style={{
                    width: '500px',
                    color: '#fff',
                    fontWeight: 500,
                    fontSize: 'medium',
                  }}
                >
                  <Col span={8}>
                    <AppstoreOutlined /> 应用管理
                  </Col>
                  <Col span={8}>
                    <UserOutlined /> 用户管理
                  </Col>
                  <Col span={8}>
                    <TeamOutlined /> 角色管理
                  </Col>
                </Row>
              </div>
              {defaultDom}
            </>
          );
        }}
        {...settings}
      >
        <PageContainer>
          <ProCard
            style={{
              minHeight: 700,
            }}
          >
            <Outlet />
          </ProCard>
        </PageContainer>
      </ProLayout>
    </div>
  );
};
