import { signIn } from '@/services/api';
import { maskingString, getUrlParams } from '@/utils/utils';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { message, Tabs } from 'antd';
import React, { useEffect, useState } from 'react';
import { ProFormCheckbox, ProFormText, LoginForm } from '@ant-design/pro-form';
import { useIntl, history, FormattedMessage, SelectLang } from '@umijs/max';
import Cookies from 'universal-cookie';

import styles from './index.less';

const tabContainerStyle = {
  width: '280px',
  minWidth: '280px',
  height: '260px',
  minHeight: '260px',
  paddingTop: '12px',
};

const SignIn: React.FC = () => {
  const [type, setType] = useState<string>('name');

  const intl = useIntl();

  useEffect(() => {
    const { appid, redirect } = getUrlParams();
    const cookies = new Cookies();
    const token = cookies.get('identify_appid_' + appid);
    if (token) {
      const params = {
        app: appid,
        type: 'token',
        redirect: redirect,
        token: token,
      };
      const call = async () => {
        const ret = await signIn(params);
        if (ret.code === 0) {
          const url = redirect + '?token=' + ret?.data?.token;
          history.push(url);
          return;
        }
      };
      call();
    }
  }, []);

  const handleSubmit = async (values: any) => {
    try {
      const { appid, redirect } = getUrlParams();
      const params = {
        app: appid,
        type: 'name',
        name: values.username,
        redirect: redirect,
        password: maskingString(values.password),
        remember: Number(values.autoLogin),
      };
      const ret = await signIn(params);
      if (ret.code === 0) {
        // set cookie
        if (params.remember !== 0) {
          const cookies = new Cookies();
          cookies.set('identify_appid_' + appid, ret?.data?.token, {
            path: '/',
            maxAge: 15 * 24 * 60 * 60,
          });
        }
        const url = redirect + '?token=' + ret?.data?.token;
        history.push(url);
        return;
      }
      console.log('sign with error:', ret);
      message.error(
        ret.code.toString() +
          '：' +
          intl.formatMessage({
            id: 'pages.signin.users.failure',
          }),
      );
    } catch (error) {
      console.log('error:', error);
      message.error(
        '-100' +
          '：' +
          intl.formatMessage({
            id: 'pages.signin.failure',
          }),
      );
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.background}></div>
      <div className={styles.lang} data-lang>
        {SelectLang && <SelectLang />}
      </div>
      <div className={styles.content}>
        <div className={styles.board}>
          <div className={styles.header}>
            <img
              alt="logo"
              className={styles.logo}
              src={require('@/assets/paper.png')}
            />
          </div>

          <div className={styles.tabs}>
            <Tabs
              centered
              activeKey={type}
              onChange={setType}
              items={[
                {
                  key: 'name',
                  label: <FormattedMessage id="pages.signin.users.tab" />,
                },
                {
                  key: 'mobile',
                  label: <FormattedMessage id="pages.signin.phone.tab" />,
                },
              ]}
            />
          </div>

          {type === 'name' && (
            <>
              <LoginForm
                initialValues={{
                  autoLogin: true,
                }}
                onFinish={async (values) => {
                  await handleSubmit(values);
                }}
                contentStyle={tabContainerStyle}
                className={styles.customInput}
              >
                <ProFormText
                  name="username"
                  fieldProps={{
                    size: 'large',
                    prefix: <UserOutlined className={styles.prefixIcon} />,
                  }}
                  placeholder={''}
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage id="pages.signin.users.username.required" />
                      ),
                    },
                  ]}
                />
                <ProFormText.Password
                  name="password"
                  fieldProps={{
                    size: 'large',
                    prefix: <LockOutlined className={styles.prefixIcon} />,
                  }}
                  placeholder={''}
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage id="pages.signin.users.password.required" />
                      ),
                    },
                  ]}
                />

                <div
                  style={{
                    textAlign: 'left',
                    marginBottom: 24,
                  }}
                >
                  <ProFormCheckbox noStyle name="autoLogin">
                    <FormattedMessage id="pages.signin.remember" />
                  </ProFormCheckbox>
                  <a
                    style={{
                      float: 'right',
                    }}
                  >
                    <FormattedMessage id="pages.signin.forgot" />
                  </a>
                </div>
              </LoginForm>
            </>
          )}

          {type === 'mobile' && (
            <>
              <div
                style={{
                  height: '260px',
                  minHeight: '260px',
                }}
              ></div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default SignIn;
