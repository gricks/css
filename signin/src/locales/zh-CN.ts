export default {
  'pages.signin.success': '登录成功',
  'pages.signin.failure': '网络异常, 请稍后再试！',
  'pages.signin.remember': '自动登录',
  'pages.signin.forgot': '忘记密码',
  'pages.signin.users.tab': '用户名登录',
  'pages.signin.users.failure': '用户名或密码错误！',
  'pages.signin.users.username.required': '账号不能为空',
  'pages.signin.users.password.required': '密码不能为空',
  'pages.signin.phone.tab': '手机号登录',
  'pages.signin.phone.failure': '手机号或验证码错误',
  'pages.signin.phone.placeholder': '手机号',
  'pages.signin.phone.required': '手机号不能为空',
  'pages.signin.phone.invalid': '手机号格式错误',
  'pages.signin.phone.captcha.placeholder': '验证码',
  'pages.signin.phone.captcha.invoke': '获取验证码',
  'pages.signin.phone.captcha.required': '验证码不能为空',
};
