import { history, Outlet, useAccess, useLocation } from '@umijs/max';
import routes from '../../config/routes';

const Auth = () => {
  const location: any = useLocation();

  const access: any = useAccess();

  const currentPathname = location.pathname.split('?')[0];

  routes.forEach((route: any) => {
    if (
      route.access &&
      currentPathname.indexOf(route.path) !== -1 &&
      !access[route.access]
    ) {
      history.push({
        pathname: '/403',
      });
    }
    if (route.routes && route.routes.length) {
      route.routes.forEach((subRoute: any) => {
        if (
          subRoute.access &&
          currentPathname.indexOf(subRoute.path) !== -1 &&
          !access[subRoute.access]
        ) {
          history.push({
            pathname: '/403',
          });
        }
      });
    }
  });
  return <Outlet />;
};

export default Auth;
