import { request } from '@umijs/max';

export async function signIn(params?: { [key: string]: any }) {
  return request<any>('/v1/signin', {
    method: 'GET',
    params: params || {},
  });
}
