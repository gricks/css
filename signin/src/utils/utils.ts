import { useEffect } from 'react';

let md5 = require('md5');

export function maskingString(params: string) {
  return md5(md5(params) + 'identify');
}

export function getUrlParams() {
  const params = new URL(window.location.href).searchParams;
  const appid = params.get('appid') || '1000';
  let redirect = params.get('redirect') || '/';
  return { appid, redirect };
}

export function useScript(url: string) {
  useEffect(() => {
    const script = document.createElement('script');
    script.src = url;
    script.async = true;
    document.body.appendChild(script);
    return () => {
      document.body.removeChild(script);
    };
  }, [url]);
}
