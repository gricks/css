const routes: any[] = [
  {
    path: '/',
    redirect: '/application',
  },
  {
    path: '/signin',
    component: '@/pages/SignIn',
  },
  {
    path: '/application',
    component: '@/pages/Application',
    wrappers: ['@/wrappers/auth', '@/layouts/BasicLayout'],
    //access: 'application.view',
    name: '应用管理',
  },
];

export default routes;
