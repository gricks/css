/*
代理配置
https://umijs.org/docs/guides/proxy
*/

export default {
  dev: {
    '/v1/': {
      target: 'http://10.10.6.50:9980/',
      changeOrigin: true,
    },
  },
};
